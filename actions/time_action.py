from st2common.runners.base_action import Action
from datetime import datetime
import requests

class schedule_time(Action):

    def run(self, date, proto, hostname, port, path, headers, data, method):
        # parse the startdate as a datetime
        start_date = datetime.strptime(str(date), "%Y-%m-%d %H:%M:%S")

        # get the difference between the start date and end date in seconds
        sleeptime = (start_date - datetime.now()).total_seconds()

        # sleep the difference in time
        time.sleep(sleeptime)

        # send http request back to resource manager
        requests.post(url = proto + hostname + ':' + str(port) + path, headers = headers, body = data, method = method)