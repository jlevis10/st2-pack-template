from st2common.runners.base_action import Action
import requests
import json
import datetime

class http_request(Action):

    def run(self,endpoint):
        start_date = datetime.strptime("%Y-%m-%d %H:%M:%S");
        response = requests.get(endpoint)
        response.raise_for_status()

        return response.json()